#!/bin/bash

set -e
export PROJECT_ID=$(gcloud config get-value core/project)
export SERVICE_ACCOUNT_NAME="gitlab-runner-borislaviliyan";
export SERVICE_ACCOUNT_EMAIL="${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"

gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME} --display-name "GitLab Runner  " || true;

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
       --member="serviceAccount:${SERVICE_ACCOUNT_EMAIL}" \
       --role='roles/owner' || echo "Failed to give service account permissions (needs owner permissions), do this manually";

kubectl create namespace gitlab-borislaviliyan || true

if kubectl describe secret google-key --namespace=gitlab-borislaviliyan >/dev/null ; then
    echo -n "";
else
    gcloud iam service-accounts keys create \
       --iam-account "${SERVICE_ACCOUNT_EMAIL}" \
       service-account.json

    kubectl create secret generic google-key --from-file service-account.json --namespace=gitlab-borislaviliyan
fi;

if kubectl get clusterrolebinding gitlab-borislaviliyan-cluster-admin; then
    echo -n ""
else
    kubectl create clusterrolebinding gitlab-borislaviliyan-cluster-admin --clusterrole=cluster-admin --serviceaccount=gitlab-borislaviliyan:default
fi;
