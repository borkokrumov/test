#!/usr/bin/env bash

export PROJECT_ID=$(gcloud config get-value core/project)
export SERVICE_ACCOUNT_NAME="cluster";
export SERVICE_ACCOUNT_EMAIL="${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"

echo $1;
echo $2;
echo $3;
echo $4;
echo $5;

gcloud container node-pools create $1 --cluster=$2 --machine-type=$3 --num-nodes=$4 --zone=$5 --service-account=${SERVICE_ACCOUNT_EMAIL} --no-enable-autoupgrade
